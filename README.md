# Django Commands Tips



## Create a New project

1.  Open your terminal and type:

   ```
   django-admin startproject project_name
   ```

2. Run your app:

   ```
   python3 manage.py runserver
   ```

   



### Create a new app in your project

1. Type:

   ```bash
   python3 manage.py startapp app_name
   ```

2. Add your app to settings, in your global path folder search and edit settings.py

   ```python
   ...
   
   INSTALLED_APPS = [
       'django.contrib.admin',
       'django.contrib.auth',
       'django.contrib.contenttypes',
       'django.contrib.sessions',
       'django.contrib.messages',
       'django.contrib.staticfiles',
       'app-name',
   ]
   
   ...
   ```

   

### URLS

To add and use a new url go to main folder path and edit urls.py

```python
...
from django.contrib import admin
from django.urls import path
from generator import views    <--- import your view or views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home), <--- and add pattern
]
...
```



* Note: Maybe you need def a new function in your app view file, like this:

  ```python
  from django.shortcuts import render
  from django.http import HttpRequest
  
  # Create your views here.
  
  def home(request):
      return HttpResponse('Hello there!')
  ```

  

### Templates

Create a new folder into `app-project` with name(lowercase) `templates`, and here you can put your's html file.

folder:

```
my_super_app
  |__ app
        |__ templates
              |__ generator          <--- can use any name
                  |
                   --> home.html
```



